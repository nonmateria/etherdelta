
Processing sketch to use an android phone as accelerometer sensor over an OSC network. Remember to set up the right port and destination IP.

to be built needs:
- Processing 3
- in processing: the Android mode and the Ketai and oscP5 libraries

build steps:
- connect the phone, check that is shown in the Android->Devices menu
- open the sketch with processing and configure the settings at the top
- run it on the device to install it (the first time will take a lot, as it will download gradle, the android emulator and the SDK, for 1.4 Gb of bloat) 

when installed: 
- connect the phone to the same network of the receiving device 
- tap three times the screen to calibrate the center

more memos for myself:
1) use 'np-hub.sh' to create hub
2) connect phone to the hub, etherdelta is already set to the right IP
3) messages are sent to port 5252 by default

to enable Android Developer Settings: go to Settings >> About Device, or Settings >> About >> Software Information >> More and tap "Build Number" 7 times.

License: Nicola Pisanti MIT License 2021
